function mapObject(obj, cb) {
  if(obj === undefined) return {};
  if(cb === undefined) return obj;
  let value = {};
  for (let val in obj) {
    value[val] = cb(obj[val]);
  }
  return value;
}


module.exports = mapObject;

function invert(obj) {
  let revObj = {};
  for (let key in obj) {
    revObj[obj[key]] = key;
  }
  return revObj;
}


module.exports = invert;

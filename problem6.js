//Solution
function defaults(obj, defaultProps) {
  let keys = Object.keys(obj);
  for(let key in defaultProps){
      if(!keys.includes(key)){
          obj[key] = defaultProps[key]
      }
  }
  return obj;
}

module.exports = defaults;
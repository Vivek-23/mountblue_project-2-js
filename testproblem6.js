const defaults = require('./problem6');
const underscore = require('underscore')
const testObject = require('./object_exercise');

let result = defaults(testObject, {location: "New York", opponent: "Joker"});
console.log(result);

let result1 = underscore.defaults(testObject, {location: "New York", opponent: "Joker"});
console.log(result1);

